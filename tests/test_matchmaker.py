#!/usr/bin/env python

import unittest
import xmlrpc
import time
from pprint import pprint

from rummatch.matchmaker import Matchmaker
from rummatch.match import GameMode, Match
from rummatch.player import Player
from fakeplayers import generate_new

import fakeplayers


class TestMatchmaker(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.mm = Matchmaker()
        # Generate fake players data
        parms_dict = {
            "rating":[1, 1000, 500, -1000, 2400],
            "cpu": [2, 50, 450]
        }
        players_data = generate_new(30,parms_dict)
        
        # Fill the queue
        for i in players_data:
            cls.mm.add_player(i["id"], i["features"])

    def test_buckets(self):
        self.assertEqual(
            tuple(self.mm.buckets.keys()),
            ("rating", "cpu"))
    
    # def test_players_matrix(self):
    #     pass

    # def test_main(self): 
    #     time.sleep(1)
    #     # mm.make_match()
    
    def test_add_player_to_bucket(self):
        player = Player(
            player_id=31,
            parms={
                "rating":673,
                "cpu":105
            })
        self.mm._assign_player_to_buckets(player)
        self.assertTrue(player in self.mm.buckets["rating"][1])
        self.assertTrue(player in self.mm.buckets["cpu"][0])


class TestPlayer(unittest.TestCase):
    pass


class TestMatch(unittest.TestCase):
    def test_1(self):
        gm = GameMode(2, 1)
        m = Match(gm)
        self.assertEqual(m.game_mode.players_num, 2)


class TestGenPlayers(unittest.TestCase):
    def test_1(self):
        d = {
            "region": [2, 0, 6],
            "skill_global": [1, 1000, 500, -1000, 2400],
            "skill_ladder": [1, 400, 250, -617, 2361],
            "games_played": [1, 2000, 2000, 0, 8000],
            "cpu": [2, 0, 450]
        }
        size = 20000
        players = fakeplayers.players.generate_new(size, d)
        self.assertIsInstance(players, list)
        self.assertIsInstance(players[0], dict)
        self.assertEqual(size, len(players))
