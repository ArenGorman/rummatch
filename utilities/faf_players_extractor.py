#!/usr/bin/env python
import time
import os
import json
from pprint import pprint
from urllib import request as rq

PAGE_SIZE = 100

PAGES_LADDER = 86
PAGES_GLOBAL = 100

URL_GLOBAL = "https://api.faforever.com/leaderboards/global?page[size]={}&page[number]={}"
URL_LADDER = "https://api.faforever.com/leaderboards/ladder1v1?page[size]={}&page[number]={}"

def parse_faf_players(rq_url:str, pages):
    raw_data = {"data":[]}
    for i in range(1, pages + 1):
        url = rq_url.format(i)
        with rq.urlopen(url) as response:
            if response.getcode() == 200:
                page_data = json.loads(response.read().decode("utf-8"))
                raw_data['data'].extend(page_data['data'])
        time.sleep(1)
    with open("tempfile.json", "w") as target:
        json.dump(raw_data, target, indent=4, sort_keys=True)



if __name__ == "__main__":
    parse_faf_players(rq_url=URL_LADDER.format(PAGE_SIZE, "{}"),
                      pages=PAGES_LADDER)
