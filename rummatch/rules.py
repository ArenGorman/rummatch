from abc import ABC, abstractmethod, abstractproperty
from typing import Iterable

class BaseRuleInterface(ABC):
    """Interface for rules"""
    def __init__(self, parameter_name):
        self.name = parameter_name

    @abstractmethod
    def matches(self, value):
        raise NotImplementedError

class NumericRule(BaseRuleInterface):
    def __init__(self, parm_name: str, minimum, maximum,
                 include:tuple=(True, True)):
        super(NumericRule, self).__init__(parm_name)
        self.min = minimum
        self.max = maximum
        self.include = include # include boundaries or not
    
    def __repr__(self):
        return "<{}.{} {}{},{}{}>".format(
            self.__class__.__module__,
            self.__class__.__qualname__,
            "[" if self.include[0] else "(",
            self.min,
            self.max,
            "]" if self.include[1] else ")",
        )

    def matches(self, value) -> bool:
        if all(self.include):
            result = all((value >= self.min, value <= self.max))
        elif self.include[0] and not self.include[1]:
            result = all((value >= self.min, value < self.max))
        elif not self.include[0] and self.include[1]:
            result = all((value > self.min, value <= self.max))
        else:
            result = all((value > self.min, value < self.max))
        return result

class ListMemberRule(BaseRuleInterface):
    def __init__(self, parm_name: str, parameter_list: Iterable):
        super(ListMemberRule, self).__init__(parm_name)
        self.parameter_list = parameter_list
    
    def matches(self, value):
        return (value in self.parameter_list)
