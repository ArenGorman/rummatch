#!/usr/bin/env python


class GameMode():
    """
    Description for game mode
    Properties:
        teams_num (int): 
        team_size (int): 
        players_num (int): teams_num * team_size
    """

    def __init__(self, teams_num: int = 2, team_size: int = 3):
        self.players_num = teams_num * team_size
        self.teams_num = teams_num
        self.team_size = team_size
        self.map_name = "Astro Crater"


class Match():
    """
    """
    def __init__(self, gm: GameMode):
        self.game_mode = gm
        self.teams = []
    
    def is_playable(self):
        if len(self.teams) == self.game_mode \
            and all([t.size() == self.game_mode.team_size for t in self.teams]):
            return True
        return False


