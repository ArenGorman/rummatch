#!/usr/bin/env python


class Team():
    def __init__(self, players: list, name:str, team_id: int):
        self.id = team_id
        self.name = name
        self.players = []

    def size(self):
        return len(self.players)
