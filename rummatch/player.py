#!/usr/bin/env python
from time import time

from .match import GameMode

class Player(object):
    """
    Properties:
        id (int): player identifier
        target_game_modes (tuple): list of game modes that player wish to play
        parms (dict): key-value storage of player's parameters (rating, ping,
            location, etc)
        time_enqueued (int): time when player was added to queue
    """

    def __init__(self,
                 player_id:  int,
                 parms:      dict,
                 time_added: int=None,
                 target_gms: tuple=()
                ):
        self.id = player_id
        # if not target_gms:
        #     raise ValueError("Target game modes must have at least one member")
        # if any([not issubclass(x.__class__, GameMode) for x in target_gms]):
        #     raise TypeError("Target game mode tuple should consist of instances"
        #                     "of proper types")
        self.target_game_modes = target_gms
        self.parms = parms
        self.time_enqueued = int(time_added) if time_added else int(time())
    
    def __repr__(self):
        return "<{}.{} id={} wt={}>".format(
            self.__class__.__module__,
            self.__class__.__qualname__,
            self.id,
            int(time() - self.time_enqueued))

    def __eq__(self, other_player):
        if self.id == other_player.id:
            return True
        return False

    def __hash__(self):
        return hash(self.id)

