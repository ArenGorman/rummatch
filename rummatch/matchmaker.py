#!/usr/bin/env python
import asyncio
import threading
import logging
import time
from xmlrpc import server
from socketserver import ThreadingMixIn

from threading import Lock
from queue import Queue

from .player import Player
from .match import Match, GameMode
from .rules import NumericRule, ListMemberRule

MAXWAITTIME = 10 * 60  # seconds
REGIONS = ("EU", "NA", "SA", "AS", "AU", "AF")

"""
0. Select map
1. Query the population on hard filters (criteria that must be met) based on
some initial criteria (gamemode, QoS targets based on server capacity available
in a region, skill banding, XP minimums)

2. Loop over players or select some players to start doing localized optimistic
selections

2.1. If necessary, proceed to do more direct comparisons (fitness weights,
player intent, predicted outcome, quality assessment)

3. If you did some parallel work, make sure players don’t appear in multiple
matches

TODO's:
0. [ ] Map selection
1. [ ] Single players matchmaking
    1.1 [ ] Hard filters - rating in range, location, team rating sum, cpu
    1.2 [ ] Soft filters - ping to each other, difference with opponent's rating
    1.3 [ ] Looping over selected for checking criterias

"""

# Multithreaded server
class RPCServerThreaded(ThreadingMixIn, server.SimpleXMLRPCServer):
    pass

class Matchmaker(object):
    """
    Properties:
        queue         (set): waiting queue
        max_wait_time (int): time for each player after which he must be matched
        game_modes  (tuple): list of possible game modes
    """

    def __init__(self, max_wait_time: int=MAXWAITTIME):
        """Args:
            max_wait_time (int): time limit that defines when player MUST be put
                to the match
        """
        self.queue = set()
        self.max_wait_time = max_wait_time
        self.game_modes = ()  # List of supported game modes
        self.queue_lock = Lock()

        # RPC
        self.server = RPCServerThreaded(('localhost', 8246))
        self.server.register_function(self.add_player)
        self.server.register_function(self.del_player)
        # self.server.serve_forever()

        # Bucket rules for hard filters
        self.rules = {
            "rating":[
                NumericRule("rating:0-500", 0, 500),
                NumericRule("rating:501-1000", 501, 1000),
                NumericRule("rating:1001-1500", 1001, 1500),
                NumericRule("rating:1501+", 1501, 10000)
            ],
            "cpu": [
                NumericRule("cpu:0_200", 0, 200),
                NumericRule("cpu:201_300", 201, 300),
                NumericRule("cpu:301+", 301, 450)
                ]
        }

        # Prepare buckets itself
        self.buckets = {}
        for k, v in self.rules.items():
            self.buckets[k] = tuple(set() for i in range(len(v)))
        

        # Soft filters
        # 0. Players' region
        # 1. ping to each other players
        # 2. opponent's rating on map
        # 3. Veterancy

    def run(self, parameter_list):
        """Main loop"""
        while True:
            # First, process the queue by hard filters, filling the buckets
            while self.queue:
                # Take each player from queue and put it to buckets
                pl = self.queue.pop()
                self._assign_player_to_buckets(pl)
            
            # START MATCHMAKING

            
            # Subtract buckets to find crossovers
            matrix = self._calculate_players_matrix()
            for row in matrix:
                for group in row:
                    print(group)
                    pass


    def _assign_player_to_buckets(self, player:Player) -> bool:
        """Look for all buckets and put player in all that matches it's parameters
        Args:
            player (Player):
        Return:
            (dict): whether player fit any bucket
        """
        index_list = {}
        for k, v in player.parms.items():
            for bucket_name in self.rules:
                if k==bucket_name:
                    index = self._find_matching_bucket(
                        v, self.rules[bucket_name])
                    if index is not None:
                        self.buckets[k][index].add(player)
                        index_list[k] = index
        if index_list:
            return True
        else:
            return False

    def _calculate_players_matrix(self):
        """Find an intersection of buckets"""
        matrix = []
        if self.buckets:
            for set_1 in self.buckets["rating"]:
                row = []
                for set_2 in self.buckets["cpu"]:
                    row.append([i for i in set_1.union(set_2)])
                # Sort by wait time
                row[-1] = sorted(row[-1], key=lambda x: x.time_enqueued)
                matrix.append(row)
            return matrix


    def _remove_player_from_buckets(self, player:Player) -> bool:
        """Used when player leaved queue or was sent to the match
        Args:
            player (Player):
        Return:
            (bool)
        """
        for bucket_set in self.buckets.values():
            for bucket in bucket_set:
                if player in bucket:
                    bucket.remove(player)
                        
    def _find_matching_bucket(self, value, bucket_list: list):
        """Iterate over list of rules checking if value matches any of the
        rules. Return index of the rule in the list if found one that matches.
        Args:
            value: parameter value which we try to fit into one of the buckets
        Return:
            (int): index of bucket that suits for this value
        """
        # print(value)
        for i, rule in enumerate(bucket_list):
            # print(rule)
            # print(rule.matches(value))
            if rule.matches(value):
                return i
        return None

    def add_player(self, player_id: int, parms: dict, target_GMs: tuple=()):
        """Add player to waiting queue"""
        # TODO: add check for parameters needed for buckets
        # for now I just suppose all players has these parms for 100% cases
        self.queue.add(
            Player(player_id, parms, time.time(), target_gms=target_GMs)
            )

    def del_player(self, player_id: int):
        player = Player(player_id, (), {})
        self._remove_player_from_buckets(player)
        if player in self.queue:
            self.queue.remove(player)


class Bucket():
    def __init__(self):
        self.players = {}
    
