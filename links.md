# Useful links

### Kernel density estimation

- https://scikit-learn.org/stable/modules/density.html
- https://mathisonian.github.io/kde/
- https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
- https://nbviewer.jupyter.org/url/mglerner.com/HistogramsVsKDE.ipynb

### Matchmakers architecture
https://www.youtube.com/watch?v=-pglxege-gU
https://www.youtube.com/watch?v=0FoG4Jtpebs

https://multiplay.com/2019/03/27/making-and-delivering-matches-part-one/
https://multiplay.com/2019/04/07/making-and-delivering-matches-part-two/