#!/usr/bin/env python
"""

"""
import numbers
from time import time_ns
from enum import Enum
from collections import namedtuple

import matplotlib
import scipy as sp
import numpy as np
from scipy import stats

# #TODO use this
# class DistrType(Enum):
#     CONTINUOUS = 1
#     DISCRETE = 2

class DistLaw(Enum):
    NORMAL = 1
    UNIFORM = 2
    GAMMA = 3
    BETA = 4
    KDE = 5
    HISTOGRAM = 6

# TODO: implement more generic approach
# TODO: use resample from KDE and histogram
def generate_new(players_num: int, parameters: dict) -> list:
    """
    Args:
        players_num (int):
        parameters (dict): parms (ping, rating, region, etc.) names and
            how it should be generated. 
            Example:
            TODO add "from data"
            {
                "region":[2, 0, 6],
                "skill_global":[
                    1,
                    1000,
                    500,
                    -1000,
                    2400
                ],
                "skill_ladder":[
                    1,
                    400,
                    250,
                    -617,
                    2361
                ],
                "games_played":[
                    1,
                    2000,
                    2000,
                    0,
                    8472
                ],
                "cpu":[2, 0, 800]
            }
            "regions":["NA","SA","EU","AS","AU","OC","AF"]
    Returns:
        result_players (list)
    """
    result_players = []
    raw_data = {}
    for k, v in parameters.items():
        if v[0] == DistLaw.NORMAL.value:
            raw_data[k] = list(sample_truncated_normal(*v[1:],
                                                       players_num))
        elif v[0] == DistLaw.UNIFORM.value:
            raw_data[k] = list(sample_uniform(*v[1:], players_num))
    generated_parms = tuple(zip(*raw_data.values()))

    for i in range(players_num):
        result_players.append(
            {
                "id": i,  # TODO proper ids to avoid collisions
                "nick": "empty",  # TODO lowpriority
                "features": dict(zip(raw_data.keys(), generated_parms[i]))
            }
        )
    return result_players


def sample_truncated_normal(mu, sigma, lower, upper, size):
    result = stats.truncnorm((lower - mu) / sigma, (upper - mu) / sigma,
                           loc=mu,
                           scale=sigma
                           ).rvs(size)
    result = [float(i) for i in result]
    return result


def sample_uniform(lower, upper, size):
    return [int(i) for i in np.random.randint(lower, upper, size)]

# def sample_continuous(law:str):
#     pass

# def sample_discrete(law:str):
#     pass

# TODO


def resample_from_histogram(dataset, size):
    pass


def resample_from_KDE(
    dataset: (list, np.ndarray),
    size: int,
    discrete: bool = False
) -> np.ndarray:
    """Calculates kernel density estimation for specified dataset
    Args:
        dataset (list): list of values
    Returns:
        np.ndarray: <size> elements of resampled from KDE values
    """
    # TODO implement proper resampling for discrete variables using actual variable domain
    # Check for dataset
    if len(dataset) > 1_000_000:
        raise ValueError("Dataset size is too big. Only up to 1 000 000 numbers"
                         "supported")
    if any(not isinstance(i, numbers.Number) for i in dataset):
        raise TypeError("Values in dataset must be numbers!")

    kde = stats.gaussian_kde(dataset)
    if discrete:
        # This is defenitely a crutch just to make it look like discrete #BUG
        result = list(map(lambda x: round(x),
                          kde.resample(size, seed=time_ns()[0])))
    else:
        result = kde.resample(size, seed=time_ns())
    return result
