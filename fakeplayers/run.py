#!/usr/bin/env python

from players import generate_new, resample_from_KDE #pylint: disable=import-error

from xmlrpc import server

if __name__ == "__main__":
    fp_server = server.SimpleXMLRPCServer(("localhost", 8000), allow_none=True)
    fp_server.register_function(generate_new)
    fp_server.register_function(resample_from_KDE)
    fp_server.serve_forever()
