#!/usr/bin/env py
"""
This module allows to create fake players with needed parameters by sampling
common distributions e.g. Normal, Uniform or Weibull distribution with needed
parameters
"""

import scipy as sp
import numpy as np

from .players import generate_new, resample_from_KDE

if __name__ == "__main__":
    # DEBUG:
    d = {"region": [2, 0, 6],
        "skill_global": [1, 1000, 500, -1000, 2400],
        "skill_ladder": [1, 400, 250, -617, 2361],
        "games_played": [1, 2000, 2000, 0, 8000],
        "cpu": [2, 0, 450]
    }
    # Same for pasting in interpreter:
    # d = {"region": [2, 0, 6],"skill_global": [1, 1000, 500, -1000, 2400],"skill_ladder": [1, 400, 250, -617, 2361],"games_played": [1, 2000, 2000, 0, 8000],"cpu": [2, 0, 450]}
    generate_new(2, d)
